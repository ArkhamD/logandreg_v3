package com.example.logandreg

import android.graphics.Color
import android.graphics.Typeface
import android.graphics.fonts.FontFamily
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import com.example.logandreg.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    lateinit var binding : FragmentHomeBinding
    private val adapter = StringAdapter()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater)
        val string = "Это\n" +
                "список\n" +
                "из\n" +
                "строк.\n"
        binding.homeText.setOnClickListener {
            binding.recyclerView.layoutManager = LinearLayoutManager(this.context)
            binding.recyclerView.adapter = adapter
            adapter.addString(string)
        }

        var i = 0
        binding.btnChangeStyle.setOnClickListener {
            binding.apply {
                when (i++ % 2) {
                    0 -> {
                        btnChangeStyle.textSize = 24f
                        btnChangeStyle.setTextColor(Color.CYAN)
                        btnChangeStyle.typeface = Typeface.DEFAULT_BOLD

                        homeText.textSize = 30f
                        homeText.setTextColor(Color.CYAN)
                        homeText.typeface = Typeface.DEFAULT_BOLD
                    }
                    1 -> {
                        btnChangeStyle.textSize = 14f
                        btnChangeStyle.setTextColor(Color.BLACK)
                        btnChangeStyle.typeface = Typeface.DEFAULT

                        homeText.textSize = 25f
                        homeText.setTextColor(Color.BLACK)
                        homeText.typeface = Typeface.DEFAULT
                    }
                }
            }
        }

        binding.navView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.profile -> {
                    Navigation.findNavController(binding.root).navigate(R.id.action_homeFragment_to_profileFragment)
                }
                R.id.web -> {
                    Navigation.findNavController(binding.root).navigate(R.id.action_homeFragment_to_webFragment)
                }
            }
            true
        }

        return binding.root
    }

}