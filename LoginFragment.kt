package com.example.logandreg

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.logandreg.databinding.FragmentLoginBinding
import java.io.*

class LoginFragment : Fragment() {

    lateinit var binding: FragmentLoginBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater)

        binding.btnToHome.setOnClickListener {
            for (lines in loadtext(this.context)!!) {
                val line = lines.split(" ")
                if (binding.enterLogin.text.toString() == line[0] && binding.enterPass.text.toString() == line[1]) {
                    Navigation.findNavController(binding.root).navigate(R.id.action_loginFragment_to_homeFragment)
                    break
                }
                else {
                    Toast.makeText(this.context, "Неверный логин/пароль" , Toast.LENGTH_SHORT).show()
                }
            }
        }
        binding.btnToReg.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_loginFragment_to_registrationFragment)
        }
        return binding.root
    }

    private fun loadtext(context: Context?) : List<String>?{
        var input : InputStream? = null
        var jsonString : String

        try {
            input = context?.assets?.open("base1.txt")

            val size = input?.available()
            val buffer = ByteArray(size!!)

            input?.read(buffer)

            jsonString = String(buffer)

            return jsonString.lines()
        } catch (e : Exception) {
            e.printStackTrace()
        } finally {
            input?.close()
        }

        return null
    }

}
