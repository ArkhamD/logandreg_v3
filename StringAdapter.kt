package com.example.logandreg

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.logandreg.databinding.JustTextBinding

class StringAdapter : RecyclerView.Adapter<StringAdapter.StringHolder>() {
    val stringList = ArrayList<String>()
    class StringHolder(item: View) : RecyclerView.ViewHolder(item) {
        val binding = JustTextBinding.bind(item)
        fun bind(str : String) = with(binding)
        {
            textId.text = str
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.just_text, parent, false)

        return StringHolder(view)
    }

    override fun onBindViewHolder(holder: StringHolder, position: Int) {
        holder.bind(stringList[position])
    }

    override fun getItemCount(): Int {
        return stringList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addString(str : String) {
        stringList.add(str)
        notifyDataSetChanged()
    }
}